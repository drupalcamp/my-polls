let mongoose = require('mongoose');

// Subdocument schema for votes
let voteSchema = new mongoose.Schema({ ip: 'String' });

// Subdocument schema for poll choices
let choiceSchema = new mongoose.Schema({
	text: String,
	votes: [voteSchema]
});

// Document schema for polls
exports.PollSchema = new mongoose.Schema({
	question: { type: String, required: true },
	choices: [choiceSchema]
});
