// Angular service module for connecting to JSON APIs
angular.module('pollServices', ['ngResource']).
	factory('Poll', function($resource) {
    console.log("GUNG:");
    console.log($resource);
		return $resource('polls/:pollId', {}, {
      // Use this method for getting a list of polls
			query: { method: 'GET', params: { pollId: 'polls' }, isArray: true }
      
		});
    
	}).
	factory('socket', function($rootScope) {
		let socket = io.connect();
    console.log("GUNG1:");
    console.log(socket);
		return {
			on: function (eventName, callback) {
	      socket.on(eventName, function () {
	        let args = arguments;
	        $rootScope.$apply(function () {
	          callback.apply(socket, args);
	        });
	      });
	    },
	    emit: function (eventName, data, callback) {
	      socket.emit(eventName, data, function () {
          console.log("GUNG2:");
          console.log(arguments);
	        let args = arguments;
	        $rootScope.$apply(function () {
	          if (callback) {
	            callback.apply(socket, args);
	          }
	        });
	      })
	    }
		};
	});
