
angular.module('polls', ['pollServices']).
  config([ '$routeProvider', ($routeProvider) => {
    $routeProvider.
    when('/polls', {
        templateUrl: 'partials/list.html',
        controller: PollListCtrl
    }).

    when('/poll/default', {
        templateUrl: 'partials/item.html',
        controller: PollItemCtrl
    }).
    
    when('/poll/:pollId', {
        templateUrl: 'partials/item.html',
        controller: PollItemCtrl
    }).
    when('/poll/admin/:pollId', {
        templateUrl: 'partials/itemadmin.html',
        controller: PollItemCtrl
    }).
    when('/new', {
        templateUrl: 'partials/new.html',
        controller: PollNewCtrl
    }).
    otherwise({
        redirectTo: '/polls'
    });
}]);
