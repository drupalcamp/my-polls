// Controller for the poll list
function PollListCtrl($scope, Poll) {
	$scope.polls = Poll.query();
}

// Controller for an individual poll
function PollItemCtrl($scope, $routeParams, socket, Poll) {
	$scope.poll = Poll.get({pollId: $routeParams.pollId});


  socket.on('myvote', (data) => {
		console.log("myvote=");
		//console.dir(data);
    console.log("routeParams=");
		console.dir($routeParams);

		if(data._id === $routeParams.pollId) {
			$scope.poll = data;
		}
	});

  socket.on('vote', (data) => {
		console.log("vote=");
		console.dir(data);
		if(data._id === $routeParams.pollId) {
			$scope.poll.choices = data.choices;
			$scope.poll.totalVotes = data.totalVotes;
		}
	});

  $scope.vote = (choiceId) => {
		let pollId = $scope.poll._id;
		//let choiceId = $scope.poll.userVote;
		//console.log($scope.poll);
		if(!choiceId){
			choiceId = $scope.poll.userVote;
		}
		console.log("choiceId=" + choiceId);
		if(choiceId) {
			let voteObj = { poll_id: pollId, choice: choiceId };
			socket.emit('send:vote', voteObj);
		} else {

			alert('You must select an option to vote for');
		}
	};

}

// Controller for creating a new poll
function PollNewCtrl($scope, $location, Poll) {
	// Define an empty poll model object
	$scope.poll = {
		question: '',
		choices: [ { text: '' }, { text: '' }, { text: '' }]
	};

	// Method to add an additional choice option
  $scope.addChoice = () => {
		$scope.poll.choices.push({ text: '' });
	};

	// Validate and save the new poll to the database
  $scope.createPoll = () => {
		let poll = $scope.poll;

		// Check that a question was provided
		if(poll.question.length > 0) {
			let choiceCount = 0;

			// Loop through the choices, make sure at least two provided
			for(let i = 0, ln = poll.choices.length; i < ln; i++) {
				let choice = poll.choices[i];

				if(choice.text.length > 0) {
					choiceCount++
				}
			}

			if(choiceCount > 1) {
				// Create a new poll from the model
				let newPoll = new Poll(poll);

				// Call API to save poll to the database
        newPoll.$save((p, res) => {
					if(!p.error) {
						// If there is no error, redirect to the main view
						$location.path('polls');
					} else {
						alert('Could not create poll');
					}
				});
			} else {
				alert('You must enter at least two choices');
			}
		} else {
			alert('You must enter a question');
		}
	};

	// list.html button "setdefault" voteSetDefault(poll._id)
  $scope.updatePoll = (pollId) => {
		let poll = $scope.poll;
		//let choiceId = $scope.poll.userVote;
		console.log('updatePoll');
		console.dir($scope.poll);
		if(!pollId){
			return;
		}
		console.log("choiceId=" + choiceId);
		if(choiceId) {
			let voteObj = { poll_id: pollId, choice: choiceId };
			socket.emit('send:vote', voteObj);
		} else {
			alert('You cannot set default poll');
		}
	};

}
