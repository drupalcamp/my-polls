// Connect to MongoDB using Mongoose
const mongoose = require('mongoose');
// let db;
// if (process.env.VCAP_SERVICES) {
//    let env = JSON.parse(process.env.VCAP_SERVICES);
//    db = mongoose.createConnection(env['mongodb-2.2'][0].credentials.url);
// } else {
//    db = mongoose.createConnection('localhost', 'pollsapp');
// }

mongoose.connect('mongodb://localhost:27017/pollsapp');

// Get Poll schema and model
const PollSchema = require('../models/Poll.js').PollSchema;
const Poll = mongoose.model('polls', PollSchema);

// Main application view
exports.index = (req, res) => {
	//res.render('index');
  res.render('index', { title: 'Polls' });
};

// JSON API for list of polls
exports.list = (req, res) => {
	// Query Mongo for polls, just get back the question text
  Poll.find({}, 'question', (error, polls) => {
		res.json(polls);
	});
};

// JSON API for getting a single poll
exports.poll = (req, res) => {
	// Poll ID comes in the URL
	let pollId = req.params.id;

	// Find the poll by its ID, use lean as we won't be changing it
  Poll.findById(pollId, '', { lean: true }, (err, poll) => {
		if(poll) {
			let userVoted = false,
					userChoice,
					totalVotes = 0;

			// Loop through poll choices to determine if user has voted
			// on this poll, and if so, what they selected
			for(c in poll.choices) {
				let choice = poll.choices[c];

				for(v in choice.votes) {
					let vote = choice.votes[v];
					totalVotes++;

					if(vote.ip === (req.header('x-forwarded-for') || req.ip)
            && req.session.id !== null   ) {
            console.log('req.session');
            console.dir(req.session);
						userVoted = true;
						userChoice = { _id: choice._id, text: choice.text };
            console.dir(userChoice);
					}
				}
			}

			// Attach info about user's past voting on this poll
			poll.userVoted = userVoted;
			poll.userChoice = userChoice;

			poll.totalVotes = totalVotes;

			res.json(poll);
		} else {
			res.json({error:true});
		}
	});
};

// JSON API for creating a new poll
exports.create = (req, res) => {
	let reqBody = req.body,
			// Filter out choices with empty text
    choices = reqBody.choices.filter((v) => { return v.text != ''; }),
			// Build up poll object to save
			pollObj = {question: reqBody.question, choices: choices};

	// Create poll model from built up poll object
	let poll = new Poll(pollObj);

	// Save poll to DB
  poll.save((err, doc) => {
		if(err || !doc) {
			throw 'Error';
		} else {
			res.json(doc);
		}
	});
};

exports.vote = (socket) => {
  socket.on('send:vote', (data) => {
    console.log('data *****************************');
    console.dir(data);
		let ip = socket.handshake.headers['x-forwarded-for'] || socket.handshake.address.address;

    Poll.findById(data.poll_id, (err, poll) => {
			let choice = poll.choices.id(data.choice);
			choice.votes.push({ ip: ip });

      poll.save((err, doc) => {
				let theDoc = {
					question: doc.question, _id: doc._id, choices: doc.choices,
					userVoted: false, totalVotes: 0
				};

				// Loop through poll choices to determine if user has voted
				// on this poll, and if so, what they selected
				for(let i = 0, ln = doc.choices.length; i < ln; i++) {
					let choice = doc.choices[i];

					for(let j = 0, jLn = choice.votes.length; j < jLn; j++) {
						let vote = choice.votes[j];
						theDoc.totalVotes++;
						theDoc.ip = ip;

						if(vote.ip === ip) {
							theDoc.userVoted = true;
							theDoc.userChoice = { _id: choice._id, text: choice.text };
						}
					}
				}

				socket.emit('myvote', theDoc);
				socket.broadcast.emit('vote', theDoc);
			});
		});
	});
};
