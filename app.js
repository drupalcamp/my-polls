/*jshint node:true*/
const express = require('express');
const favicon = require('serve-favicon');
const logger = require('morgan');
const methodOverride = require('method-override');
const bodyParser = require('body-parser');
const multer = require('multer');
const errorHandler = require('errorhandler');
const routes = require('./routes');
const http = require('http');
const path = require('path');
const session = require('client-sessions');
const port = 5000;

const app = express();
const server = http.createServer(app);
const io = require('socket.io')(server);

/* 
// OLD NodeJS and Express;
app.set('port', process.env.VCAP_APP_PORT || port);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));
*/

// NEW NodeJS 16.14.2
app.set('port', process.env.PORT || port);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(methodOverride());

// parse application/json
app.use(bodyParser.json());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// parse multipart/form-data
// app.use(multer());
app.use(multer({ dest: './uploads/' }).single('photo'));

app.use(express.static(path.join(__dirname, 'public')));
// END NEW

app.set('trust proxy', 1) // trust first proxy

// app.use(session({
//   resave: true, 
//   saveUninitialized: true,
//   secret: 'uwotm8'
// }));
// Set up session
app.use(session({
  secret: 'GUNG_POLL',
  resave: true,
  saveUninitialized: true,
  cookie: { secure: false }
}))

// Handle Errors gracefully
app.use((err, req, res, next) => {
	if(!err) return next();
	console.log(err.stack);
	res.json({error: true});
});



// Main App Page
app.get('/', routes.index);

// MongoDB API Routes
app.get('/polls/polls', routes.list);
app.get('/polls/:id', routes.poll);
app.post('/polls', routes.create);
app.post('/vote', routes.vote);

io.sockets.on('connection', routes.vote);

//console.log('Express server listening on port ' + app.get('port'));

server.listen(app.get('port'), () => {
  console.log(app);
  console.log('LiveTV Poll server listening on port ' + app.get('port'));
});
