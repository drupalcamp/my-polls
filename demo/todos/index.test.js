// import Todos from './index';
const Todos = require('./index').Todos;
const assert = require('assert').strict;
const fs = require('fs');

/*
describe("Test Gung Todos", function() {
  it("shoudl be able to add and complet TODOs", function() {
    let todos = new Todos();
    // todos.add("make up bed");

    todos.add("run code");
    // assert.strictEqual(todos.list().length, 1);
    // assert.deepStrictEqual(todos.list(), [ {
    //    title: "run code", completed: false } 
    //   ]);
    
    todos.add("test everything");

    // assert.strictEqual(todos.list().length, 3);
    // assert.deepStrictEqual(todos.list(),
    //   [
    //     { title: "run code", completed: false },
    //     { title: "test everything", completed: false }
    //   ]
    // );

    todos.complete("run code");
    assert.deepStrictEqual(todos.list(),
      [
        { title: "run code", completed: true },
        { title: "test everything", completed: false }
      ]
    );


  });

});

describe("Test complete()", function () {
  it("should fail if there are no TODOs", function () {
    let todos = new Todos();
    const expectedError = new Error("You have no TODOs stored. Why don't you add one first?");

    assert.throws(() => {
      todos.complete("doesn't exist");
    }, expectedError);
  });
});

describe("Callback: saveToFile()", function() {
  // The done() callback function tell it() when an asynchronous function is completed.
  it("should save a single TODO", function(done) {
    let todos = new Todos();
    todos.add("Save a CSV");
    // The err is the callback function.
    todos.saveToFile((err) => {
      // console.log(process.cwd());
      assert.strictEqual(fs.existsSync('todos.csv'), true);
      let expetedFileContents = "Title,Completed\nSave a CSV,false\n";
      let content = fs.readFileSync('todos.csv').toString();
      assert.strictEqual(content, expetedFileContents);
      done(err);
    });
  });
});

describe("Promises: saveToFile2()", function() {
  it("Promises: should save a single TODO", function() {
    let todos = new Todos();
    todos.add("Save a CSV");
    // the() function
    return todos.saveToFile2().then(() => {
      assert.strictEqual(fs.existsSync('fs2-todos.csv'), true);
      let expetedFileContents = "Title,Completed\nSave a CSV,false\n";
      let content = fs.readFileSync('fs2-todos.csv').toString();
      assert.strictEqual(content, expetedFileContents);
    });
  });
});
*/
describe("Async/await: saveToFile3()", function() {

  beforeEach(function () {
    this.todos = new Todos();
    this.todos.add("Save a CSV");
  });

  afterEach(function () {
    if (fs.existsSync("fs3-todos.csv")) {
      fs.unlinkSync("fs3-todos.csv");
    }
  });

  it("Async/await: should save a single TODO", async function () {
    await this.todos.saveToFile3();

    assert.strictEqual(fs.existsSync('fs3-todos.csv'), true);
    let expetedFileContents = "Title,Completed\nSave a CSV,false\n";
    let content = fs.readFileSync('fs3-todos.csv').toString();
    assert.strictEqual(content, expetedFileContents);

    
  });

  it("Async/await: should save a single TODO completed", async function () {
    this.todos.complete("Save a CSV");
    await this.todos.saveToFile3();

    assert.strictEqual(fs.existsSync('fs3-todos.csv'), true);
    let expetedFileContents = "Title,Completed\nSave a CSV,true\n";
    let content = fs.readFileSync('fs3-todos.csv').toString();
    assert.strictEqual(content, expetedFileContents);


  });


});
