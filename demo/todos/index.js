const fs = require('fs');
const fs2 = require('fs').promises;
class Todos {
  constructor() {
    this.todos = [];
  }

  list() {
    return [...this.todos];
  }

  add(titlePar) {
    let todoObj = {
      title: titlePar,
      completed: false,
    }
    this.todos.push(todoObj);
  }

  complete(titlePar) {
    if (this.todos.length === 0) {
      throw new Error("You have no TODOs stored. Why don't you add one first?");
    }

    let todoFound = false;
    this.todos.forEach((todoObj) => {
      if (todoObj.title === titlePar ) {
        todoObj.completed = true;
        todoFound = true;
        return;
      }
    });
    if (!todoFound) {
      throw new Error(`No TODO was found with the title: ${titlePar}`);
    }
  }

  // Callback.
  saveToFile(callback) {
    let fileContents = 'Title,Completed\n';
    this.todos.forEach((todo => {
      fileContents += `${todo.title},${todo.completed}\n`;
    }));
    fs.writeFile('todos.csv', fileContents, callback);
  }

  // Promises.
  saveToFile2() {
    let fileContents = 'Title,Completed\n';
    this.todos.forEach((todo => {
      fileContents += `${todo.title},${todo.completed}\n`;
    }));
    return fs2.writeFile('fs2-todos.csv', fileContents); // no callback function.
  }

  // Promises.
  saveToFile3() {
    let fileContents = 'Title,Completed\n';
    this.todos.forEach((todo => {
      fileContents += `${todo.title},${todo.completed}\n`;
    }));
    return fs2.writeFile('fs3-todos.csv', fileContents); // no callback function.
  }
  

}

// module.exports = Todos; // TypeError: Todos is not a constructor
module.exports.Todos = Todos;
