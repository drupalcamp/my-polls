const colors = require('colors');

const chosenColor = colors.getRandomColor();
console.log(`You should use ${chosenColor.name} on your website. It's HTML code is ${chosenColor.code}`);

const favoritecolor = colors.getBlue();
console.log(`My favorite color is ${favoritecolor.name} ${favoritecolor.code}. `);
