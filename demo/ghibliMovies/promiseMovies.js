const axios = require('axios');
const fs = require('fs').promises;

axios.get('https://ghibliapi.herokuapp.com/films')
  .then((response) => {
    console.log('* Successfully retrieved our list of moviews');
    let movieList = '';
    response.data.forEach(movie => {
      // console.log(`${movie['title']}, ${movie['release_date']}`);
      movieList += `| ${movie['title']}, ${movie['release_date']}\n`;
    })
    return fs.writeFile('PromiseMovies.csv', movieList);
  })
  .then(() => {
    console.log('- Save our list of moviews to promiseMovies.csv.')
  })
  .catch((error) => {
    console.error(`Could not sae the movies to a file: ${error}!`);
  });
