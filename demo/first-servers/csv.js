const http = require('http');
const host = 'localhost';
const port = 5001;

const requestListener = function (req, res) {
  res.setHeader("Content-Type", "text/csv");
  res.setHeader("Content-Disposition", "attachment;filename=oceanpals.csv");
  res.writeHead(200);
  res.end(`id,name,email\n1,Gung Wang,gung@gungwang.com`);
};

const server = http.createServer(requestListener);
// 3 args: port, host, and a callback function that fires
server.listen(port, host, () => {
  console.log(`Server is running on http://${host}:${port}`);
});
