console.log("Hello World");
// console.log("--------------------");
// console.log(process.argv);
// console.log("--------------------");
// console.log(process.argv.slice(2));
// console.log("--------------------");
// console.log(process.env);
console.log("--------------------");
// console.log(process.env[ "HOME" ]);
console.log("--------------------");
const args = process.argv.slice(2);
// console.log(process.env[ args[ 0 ] ]);
console.log("--------------------");

// args.forEach(arg => {
//   console.log(process.env[ arg ]);
// });

args.forEach(arg => {
  let envVar = process.env[ arg ];
  if (envVar === undefined) {
    console.error(`Could not find "${arg}" in environment`);
  } else {
    console.log(envVar);
  }
});

console.log("--------------------");

console.log("--------------------");

console.log("--------------------");

console.log("--------------------");
